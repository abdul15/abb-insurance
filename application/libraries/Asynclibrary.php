<?php


class Asynclibrary
{

	public function __construct()
	{
		$this->ci = &get_instance();
		$this->ci->load->database();
		$this->ci->load->helper('crypt');
		$this->ci->load->helper('string');

		ini_set('max_execution_time', 3000);
		ini_set('memory_limit', '1024M');
		ini_set('sqlsrv.ClientBufferMaxKBSize', '524288');
		ini_set('pdo_sqlsrv.client_buffer_max_kb_size', '524288');
	}

	function do_in_background_kartu_ab($data)
	{
		$target_dir = "./assets/qr/" . 'QR_ABB_' . date('d_F_Y') . "/";
		if (!file_exists($target_dir)) {
			mkdir($target_dir, 0777);
		}

		$convReg_a = (int) $data['register_start_ab'];


		try {
			for ($i = $data['register_start_ab']; $i <= $data['totalkartu_a']; $i++) {
				$this->ci->load->library('ciqrcode'); //pemanggilan library QR CODE

				$code = str_pad($i, 12, "0", STR_PAD_LEFT);

				$config['cacheable']    = true; //boolean, the default is true
				$config['cachedir']     = './assets/'; //string, the default is application/cache/
				$config['errorlog']     = './assets/'; //string, the default is application/logs/
				$config['imagedir']     = $target_dir; //direktori penyimpanan qr code
				$config['quality']      = true; //boolean, the default is true
				$config['size']         = '1024'; //interger, the default is 1024
				$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
				$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
				$this->ci->ciqrcode->initialize($config);

				$image_name = JENIS_KARTU_A . '_' . $code . '.png'; //buat name dari qr code sesuai dengan nim

				$params['data'] =  encrypt($code.$data['no_pengadaan'].','.JENIS_KARTU_A); //data yang akan di jadikan QR CODE and you can insert url to get this detail information
				$params['level'] = 'H'; //H=High
				$params['size'] = 10;
				$params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
				$this->ci->ciqrcode->generate($params); // fungsi untuk generate QR CODE


				$dataKartu = array(
					"no_register"		=> $code,
					"jenis_kartu"		=> JENIS_KARTU_A,
					"no_pengadaan"		=> $data['no_pengadaan'],
					"created_date"		=> date('Y-m-d h:i:s'),
					"qr_name"			=> $image_name,
					"flag_card"			=> FLAG_KARTU_CETAK,
					"token_card"		=> encrypt($code.$data['no_pengadaan'].JENIS_KARTU_A),
					"folder_name"		=> 'QR_ABB_' . date('d_F_Y') 
				);

				$this->ci->db->insert('transaksi.kartu', $dataKartu);
			}
		} catch (\Throwable $th) {
			print_r("Error" + $th);
		}
	}

	function do_in_background_kartu_c($data)
	{
		$target_dir = "./assets/qr/" . 'QR_ABB_' . date('d_F_Y') . "/";
		if (!file_exists($target_dir)) {
			mkdir($target_dir, 0777);
		}

		try {
			for ($i = $data['register_start_c']; $i <= $data['register_end_c']; $i++) {
				$this->ci->load->library('ciqrcode'); //pemanggilan library QR CODE
				$code = str_pad($i, 12, "0", STR_PAD_LEFT);

				$config['cacheable']    = true; //boolean, the default is true
				$config['cachedir']     = './assets/'; //string, the default is application/cache/
				$config['errorlog']     = './assets/'; //string, the default is application/logs/
				$config['imagedir']     = $target_dir; //direktori penyimpanan qr code
				$config['quality']      = true; //boolean, the default is true
				$config['size']         = '1024'; //interger, the default is 1024
				$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
				$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
				$this->ci->ciqrcode->initialize($config);

				$image_name = JENIS_KARTU_C . '_' . $code . '.png'; //buat name dari qr code sesuai dengan nim

				$params['data'] =  encrypt($code.$data['no_pengadaan'].','.JENIS_KARTU_A); //data yang akan di jadikan QR CODE and you can insert url to get this detail information
				$params['level'] = 'H'; //H=High
				$params['size'] = 10;
				$params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
				$this->ci->ciqrcode->generate($params); // fungsi untuk generate QR CODE


				$dataKartu = array(
					"no_register"		=> $code,
					"jenis_kartu"		=> JENIS_KARTU_C,
					"no_pengadaan"		=> $data['no_pengadaan'],
					"created_date"		=> date('Y-m-d h:i:s'),
					"qr_name"			=> $image_name,
					"flag_card"			=> FLAG_KARTU_CETAK,
					"token_card"		=> encrypt($code.$data['no_pengadaan'].JENIS_KARTU_C),
					"folder_name"		=> 'QR_ABB_' . date('d_F_Y') 
				);

				$this->ci->db->insert('transaksi.kartu', $dataKartu);
			}
		} catch (\Throwable $th) {
			print_r("Error" + $th);
		}
	}
}
