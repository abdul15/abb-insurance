<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<?php $this->load->view('_component/head.php'); ?>
<!-- END: Head-->

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">
    <!-- BEGIN: Header-->
    <?php $this->load->view('_component/header.php'); ?>
    <!-- END: Header-->

    <!-- BEGIN: SideNav-->
    <?php $this->load->view('_component/sidenav.php'); ?>
    <!-- END: SideNav-->
    <!-- END: Page Main-->
    <div id="main">
        <div class="row">
            <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
            <div class="col s12">
                <div class="container">
                    <?php $this->load->view($section); ?>
                    <!-- START RIGHT SIDEBAR NAV -->
                    <?php $this->load->view('_component/rightsidebarnav.php'); ?>
                    <!-- END RIGHT SIDEBAR NAV -->
                    <!-- Intro -->

                    <?php $this->load->view('_component/intro.php'); ?>
                    <!-- / Intro -->
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
    <!-- BEGIN: Footer-->

    <?php $this->load->view('_component/footer.php'); ?>
    <!-- END: Footer-->
    <?php $this->load->view('_component/jsfoot.php'); ?>
</body>
</html>