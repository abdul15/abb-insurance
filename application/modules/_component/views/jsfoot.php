 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url();?>app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url();?>app-assets/vendors/chartjs/chart.min.js"></script>
    <script src="<?php echo base_url();?>app-assets/vendors/chartist-js/chartist.min.js"></script>
    <script src="<?php echo base_url();?>app-assets/vendors/chartist-js/chartist-plugin-tooltip.js"></script>
    <script src="<?php echo base_url();?>app-assets/vendors/chartist-js/chartist-plugin-fill-donut.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url();?>app-assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>app-assets/js/search.js"></script>
    <script src="<?php echo base_url();?>app-assets/js/custom/custom-script.js"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url();?>app-assets/js/scripts/dashboard-modern.js"></script>
    <script src="<?php echo base_url();?>app-assets/js/scripts/intro.js"></script>
    <!-- END PAGE LEVEL JS-->