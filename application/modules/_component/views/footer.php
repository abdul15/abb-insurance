 <!-- BEGIN: Footer-->

 <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
        <div class="footer-copyright">
            <div class="container"><span>&copy; 2020 <a href="https://abb.co.id" target="_blank">ASURANSI BHAKTI BHAYANGKARA</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="mailto:div.itabb@gmail.com?subject=Info&cc=it@abb.co.id">DIVISI IT</a></span></div>
        </div>
    </footer>